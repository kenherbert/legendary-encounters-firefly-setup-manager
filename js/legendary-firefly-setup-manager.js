/* global $ */

let characters = [];
let chart = null;


/**
 * Check if elements need dark mode styling
 */
function respectDarkMode() {
    if($('body').hasClass('dark-mode')) {
//        $('#ad-catalog-card, #pending-rewards, #ads-received').addClass('bg-dark text-white');
//        $('#ad-catalog-table').addClass('table-dark');
    } else {
//        $('#ad-catalog-card, #pending-rewards, #ads-received').removeClass('bg-dark text-white');
//        $('#ad-catalog-table').removeClass('table-dark');
    }
}



function getRandomItem(array) {
    let random = Math.floor(Math.random() * array.length);

    return array[random];
}


function saveState() {
    // Not grabbed from getOptions() so we have the raw values without effect of other selections
    const playerCount = $('input[name="player-count"]:checked').val();
    const limitObjectives = $('#limit-objectives-yes').is(':checked') ? "yes" : "no";
    const episodeGroup = $('#episode-group').val();
    const limitCharacters = $('#limit-characters-yes').is(':checked') ? "yes" : "no";

    localStorage.setItem('config', JSON.stringify({
        playerCount: playerCount,
        limitObjectives: limitObjectives,
        episodeGroup: episodeGroup,
        limitCharacters: limitCharacters
    }));

    $('#save').removeClass('btn-dark').addClass('btn-success');

    setTimeout(() => {
        $('#save').removeClass('btn-success').addClass('btn-dark');
        $('#load').removeAttr('disabled');
    }, 600);
}


function loadState() {
    const options = JSON.parse(localStorage.getItem('config'));

    if(options !== null) {
        $('#load').removeAttr('disabled');

        if(options.playerCount) {
            $('input[name="player-count"]').removeAttr('checked').parent().removeClass('active');
            $(`input[name="player-count"][value="${options.playerCount}"]`).prop('checked', true).parent().addClass('active');
        }
        if(options.limitObjectives) {
            $('input[name="limit-objectives"]').removeAttr('checked').parent().removeClass('active');
            $(`#limit-objectives-${options.limitObjectives}`).prop('checked', true).parent().addClass('active');
        }

        if(options.episodeGroup) {
            $('#episode-group').val(options.episodeGroup);
        }

        if(options.limitCharacters) {
            $('input[name="limit-characters"]').removeAttr('checked').parent().removeClass('active');
            $(`#limit-characters-${options.limitCharacters}`).prop('checked', true).parent().addClass('active');
        }

        $('input[name="player-count"]:checked').trigger('change');
        $('input[name="limit-objectives"]:checked').trigger('change');
    }
}


function getOptions() {
    const limitObjectives = $('#limit-objectives-yes').is(':checked');
    const limitCharacters = limitObjectives && $('#limit-characters-yes').is(':checked');
    const episodeGroup = parseInt($('#episode-group').val(), 10);
    const playerCount = parseInt($('input[name="player-count"]:checked').val(), 10);

    return {
        limitObjectives: limitObjectives,
        limitCharacters: limitCharacters,
        episodeGroup: episodeGroup,
        playerCount: playerCount
    };
}


function generateGame() {
    const options = getOptions();
    const group = getRandomItem(data.objectives);

    let possibleFirstObjectives = data.objectives.filter(obj => obj.stage === 1);

    if(options.episodeGroup !== -1) {
        possibleFirstObjectives = data.objectives.filter(obj => obj.stage === 1 && obj.group === options.episodeGroup);
    }

    let objective1 = getRandomItem(possibleFirstObjectives);
    let objective2 = null;
    let objective3 = null;

    if(options.limitObjectives) {
        objective2 = data.objectives.filter(obj => obj.stage === 2 && obj.group === objective1.group)[0];
        objective3 = data.objectives.filter(obj => obj.stage === 3 && obj.group === objective1.group)[0];
    } else {
        let possibleSecondObjectives = data.objectives.filter(obj => obj.stage === 2);
        let possibleThirdObjectives = data.objectives.filter(obj => obj.stage === 3);
        objective2 = getRandomItem(possibleSecondObjectives);
        objective3 = getRandomItem(possibleThirdObjectives);
    }

    $('#selected-objective-1').text(objective1.name);
    $('#selected-objective-2').text(objective2.name);
    $('#selected-objective-3').text(objective3.name);

    if(options.playerCount > 1) {
        if(options.playerCount === 2) {
            $('.objective-side-job-plural').addClass('hidden');
        } else {
            $('.objective-side-job-plural').removeClass('hidden');
        }

        $('.objective-side-job-count').text(options.playerCount - 1);
        $('.objective-side-jobs').removeClass('hidden');
    } else {
        $('.objective-side-jobs').addClass('hidden');
    }

    
    if(options.playerCount > 3) {
        $('#preparation-turn-count').text(options.playerCount - 3);
        $('#preparation-turn').removeClass('hidden');

        if(options.playerCount === 5) {
            $('#preparation-turn-plural').removeClass('hidden');
        } else {
            $('#preparation-turn-plural').addClass('hidden');
        }
    } else {
        $('#preparation-turn').addClass('hidden');
    }


    let possibleCharacters = JSON.parse(JSON.stringify(data.characters));
    
    const $template = $('#player-template').children().first();
    $('#players, #main-characters').empty();

    let players = [];
    let mainCharacters = [];
    characters = [];

    while((players.length + mainCharacters.length) < 5) {
        let random = Math.floor(Math.random() * possibleCharacters.length);

        let randomCharacter = possibleCharacters.splice(random, 1)[0];

        if(!options.limitCharacters || (options.limitCharacters && (randomCharacter.recommendedGroups & objective1.group) === objective1.group)) {

            if(players.length < options.playerCount) {
                players.push(randomCharacter);
            } else {
                mainCharacters.push(randomCharacter);
            }
        } else {
            characters.push(randomCharacter);
        }
    }

    characters = characters.concat(possibleCharacters);

    players.forEach((player, index) => {
        let $character = $template.clone();

        $character.find('.player-number').text(index + 1);
        $character.find('.player-avatar').text(player.name);

        $('#players').append($character);
    });


    $('#main-characters').text(mainCharacters.map(char => char.name).join(', '));
    $('#supporting-characters').text(characters.map(char => char.name).join(', '));

    if(mainCharacters.length > 0) {
        $('#main-characters-container').removeClass('hidden');
    } else {
        $('#main-characters-container').addClass('hidden');
    }

    $('#supporting-characters-container').removeClass('hidden');

    $('#output').removeClass('hidden');
}


function updateOptionVisibility(evt) {
    if($(evt.target).is('#limit-objectives-yes')) {
        $('#limit-crew-container, #episode-group-container').removeClass('hidden');
    } else {
        $('#limit-crew-container, #episode-group-container').addClass('hidden');
    }
}


function drawChart() {
    if(chart === null) {
        chart = new google.visualization.PieChart(document.getElementById('piechart'));
    }

    const classes = {
        intel: 0,
        leadership: 0,
        strength: 0,
        survival: 0,
        tech: 0
    };

    characters.forEach(char => {
        Object.keys(char.classes).forEach(thisClass => {
            classes[thisClass] += char.classes[thisClass];
        });
    });

    
    const data = google.visualization.arrayToDataTable([
        ['Class', 'Card count'],
        ['Intel', classes.intel],
        ['Leadership', classes.leadership],
        ['Strength', classes.strength],
        ['Survival', classes.survival],
        ['Tech', classes.tech]
      ]);

      var options = {
        colors: [
            '#ffc300',
            '#00aaff',
            '#00aa00',
            '#cc0000',
            '#aaaaaa'
        ]
      };

      chart.draw(data, options);

}

function showClassDistribution() {
    $('#class-distribution-modal').modal('show');
    drawChart();
}


/**
 * Initialize the state of inputs
 */
function initialize() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(() => {
        respectDarkMode();
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });

        $('#save').click(saveState);
        $('#load').click(loadState);
        $('#generate').click(generateGame);
        $('#view-classes').click(showClassDistribution);
        $('input[name="limit-objectives"]').change(updateOptionVisibility);

        loadState();
        populateChangelog($('#changelog-accordion'), $('#changelog-template'), $('#last-updated'), changelog);
        setInterval(respectDarkMode, 1000);
    });
}


initialize();
