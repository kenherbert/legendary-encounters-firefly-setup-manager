const GROUP = {
    A: 1,
    B: 2,
    C: 4,
    D: 8,
    E: 16
};


const data = {
    objectives: [
        {
            name: 'Serenity (Part 1)',
            group: GROUP.A,
            stage: 1
        },
        {
            name: 'Serenity (Part 2)',
            group: GROUP.A,
            stage: 2
        },
        {
            name: 'The Train Job',
            group: GROUP.A,
            stage: 3
        },
        {
            name: 'Bushwhacked',
            group: GROUP.B,
            stage: 1
        },
        {
            name: 'Shindig',
            group: GROUP.B,
            stage: 2
        },
        {
            name: 'Safe',
            group: GROUP.B,
            stage: 3
        },
        {
            name: 'Our Mrs. Reynolds',
            group: GROUP.C,
            stage: 1
        },
        {
            name: 'Jaynestown',
            group: GROUP.C,
            stage: 2
        },
        {
            name: 'Out of Gas',
            group: GROUP.C,
            stage: 3
        },
        {
            name: 'Ariel',
            group: GROUP.D,
            stage: 1
        },
        {
            name: 'War Stories',
            group: GROUP.D,
            stage: 2
        },
        {
            name: 'Trash',
            group: GROUP.D,
            stage: 3
        },
        {
            name: 'The Message',
            group: GROUP.E,
            stage: 1
        },
        {
            name: 'Heart of Gold',
            group: GROUP.E,
            stage: 2
        },
        {
            name: 'Objects in Space',
            group: GROUP.E,
            stage: 3
        }
    ],
    characters: [
        {
            name: 'Book',
            recommendedGroups: GROUP.A | GROUP.B | GROUP.D,
            classes: {
                intel: 8,
                leadership: 6,
                strength: 0,
                survival: 0,
                tech: 0
            }
        },
        {
            name: 'Inara',
            recommendedGroups: GROUP.B | GROUP.C | GROUP.E,
            classes: {
                intel: 8,
                leadership: 0,
                strength: 0,
                survival: 6,
                tech: 0
            }
        },
        {
            name: 'Jayne',
            recommendedGroups: GROUP.A | GROUP.C | GROUP.D,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 8,
                survival: 0,
                tech: 6
            }
        },
        {
            name: 'Kaylee',
            recommendedGroups: GROUP.A | GROUP.D,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 0,
                survival: 6,
                tech: 8
            }
        },
        {
            name: 'Mal',
            recommendedGroups: GROUP.A | GROUP.C | GROUP.E,
            classes: {
                intel: 0,
                leadership: 6,
                strength: 0,
                survival: 8,
                tech: 0
            }
        },
        {
            name: 'River',
            recommendedGroups: GROUP.B | GROUP.D | GROUP.E,
            classes: {
                intel: 6,
                leadership: 0,
                strength: 8,
                survival: 0,
                tech: 0
            }
        },
        {
            name: 'Simon',
            recommendedGroups: GROUP.B | GROUP.C | GROUP.D,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 6,
                survival: 8,
                tech: 0
            }
        },
        {
            name: 'Wash',
            recommendedGroups: GROUP.B | GROUP.E,
            classes: {
                intel: 6,
                leadership: 0,
                strength: 0,
                survival: 0,
                tech: 8
            }
        },
        {
            name: 'Zoe',
            recommendedGroups: GROUP.A | GROUP.C | GROUP.E,
            classes: {
                intel: 0,
                leadership: 8,
                strength: 6,
                survival: 0,
                tech: 0
            }
        }
    ],
    objectiveSetup: {
        1: {
            objective1SideJobs: 0,
            objective2SideJobs: 0,
            objective3SideJobs: 0,
            setupRounds: 0
        },
        2: {
            objective1SideJobs: 0,
            objective2SideJobs: 1,
            objective3SideJobs: 2,
            setupRounds: 0
        },
        3: {
            objective1SideJobs: 2,
            objective2SideJobs: 3,
            objective3SideJobs: 4,
            setupRounds: 0
        },
        4: {
            objective1SideJobs: 4,
            objective2SideJobs: 5,
            objective3SideJobs: 6,
            setupRounds: 0
        },
        5: {
            objective1SideJobs: 4,
            objective2SideJobs: 5,
            objective3SideJobs: 6,
            setupRounds: 1
        }
    }
};


const changelog = [
    {
        date: '2023-06-14',
        items : [
            'Tooltip triggers now accept tab focus',
            'Modified changelog to be easier to maintain',
            'Added last updated date to page'
        ]
    },
    {
        date: '2023-05-07',
        items : [
            'Initial release'
        ]
    }
];