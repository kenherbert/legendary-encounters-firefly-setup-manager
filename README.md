# Legendary Encounters: Firefly setup manager

An automated game setup manager for Legendary Encounters: Firefly.

Please note that this was made for a section of the HTML code to be copy/pasted into a flatpage in my [Django portfolio](https://gitlab.com/kenherbert/developer-portfolio) but is setup as a standalone page with no requirement for Python or Django for completeness.
